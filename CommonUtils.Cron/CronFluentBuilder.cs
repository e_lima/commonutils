﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonUtils.Cron
{
    public class CronFluentBuilder
    {
        private List<ushort> Minutes { get; set; }
        private List<ushort> Hours { get; set; }
        private List<ushort> DaysOfMonth { get; set; }
        private List<ushort> Months { get; set; }
        private List<ushort> DaysOfWeek { get; set; }

        public CronFluentBuilder()
        {
            this.Minutes = new List<ushort>();
            this.Hours = new List<ushort>();
            this.DaysOfMonth = new List<ushort>();
            this.DaysOfWeek = new List<ushort>();
            this.Months = new List<ushort>();
        }

        public static CronFluentBuilder Build()
        {
            return new CronFluentBuilder();
        }

        public CronFluentBuilder SetMinutes(params string[] mins)
        {
            foreach (var item in mins)
            {
                var r = CronParser.Instance.ParseRangeExpr(CronParser.MINUTE_RANGE, item, ushort.TryParse);
                this.Minutes.AddRange(r);
            }

            return this;
        }

        public CronFluentBuilder SetMinutes(params ushort[] mins)
        {
            this.Minutes.AddRange(mins);
            return this;
        }

        public CronFluentBuilder SetHours(params string[] hours)
        {
            foreach (var item in hours)
            {
                var r = CronParser.Instance.ParseRangeExpr(CronParser.HOUR_RANGE, item, ushort.TryParse);
                this.Hours.AddRange(r);
            }

            return this;
        }

        public CronFluentBuilder SetHours(params ushort[] hours)
        {
            this.Hours.AddRange(hours);
            return this;
        }

        public CronFluentBuilder SetDaysOfMonth(params ushort[] dom)
        {
            this.DaysOfMonth.AddRange(dom);
            return this;
        }

        public CronFluentBuilder SetDaysOfMonth(params string[] dom)
        {
            foreach (var item in dom)
            {
                var r = CronParser.Instance.ParseRangeExpr(CronParser.DAY_RANGE, item, ushort.TryParse);
                this.DaysOfMonth.AddRange(r);
            }

            return this;
        }

        public CronFluentBuilder SetDaysOfWeek(params string[] dow)
        {
            foreach (var item in dow)
            {
                var r = CronParser.Instance.ParseRangeExpr(CronParser.DOW_RANGE, item, ushort.TryParse);
                this.DaysOfWeek.AddRange(r);
            }

            return this;
        }

        public CronFluentBuilder SetDaysOfWeek(params ushort[] dow)
        {
            this.DaysOfWeek.AddRange(dow);
            return this;
        }

        public CronFluentBuilder SetDaysOfWeek(params DayOfWeek[] dow)
        {
            var r = new List<ushort>();

            foreach (var item in dow)
            {
                switch (item)
                {
                    case DayOfWeek.Friday:
                        r.Add(5);
                        break;
                    case DayOfWeek.Monday:
                        r.Add(1);
                        break;
                    case DayOfWeek.Saturday:
                        r.Add(6);
                        break;
                    case DayOfWeek.Sunday:
                        r.Add(0);
                        break;
                    case DayOfWeek.Thursday:
                        r.Add(4);
                        break;
                    case DayOfWeek.Tuesday:
                        r.Add(2);
                        break;
                    case DayOfWeek.Wednesday:
                        r.Add(3);
                        break;
                    default:
                        break;
                }
            }

            this.DaysOfWeek.AddRange(r);

            return this;
        }

        public CronFluentBuilder SetMonths(params ushort[] m)
        {
            this.Months.AddRange(m);
            return this;
        }

        public CronFluentBuilder SetMonths(params string[] m)
        {
            foreach (var item in m)
            {
                var r = CronParser.Instance.ParseRangeExpr(CronParser.MONTH_RANGE, item, ushort.TryParse);
                this.Months.AddRange(r);
            }

            return this;
        }

        public CronFluentBuilder AddMinutes(params ushort[] m)
        {
            this.Minutes.AddRange(m);
            return this;
        }

        public CronFluentBuilder AddHours(params ushort[] m)
        {
            this.Hours.AddRange(m);
            return this;
        }

        public CronFluentBuilder AddDaysOfMonth(params ushort[] m)
        {
            this.DaysOfMonth.AddRange(m);
            return this;
        }

        public CronFluentBuilder AddMonths(params ushort[] m)
        {
            this.Months.AddRange(m);
            return this;
        }

        public CronFluentBuilder AddDaysOfWeek(params ushort[] m)
        {
            this.DaysOfWeek.AddRange(m);
            return this;
        }

        public CronInfo GetCronInfo()
        {
            return CronParser.ParseExpr(this.GetExpr());
        }

        public string GetExpr()
        {
            var fmt = "{0} {1} {2} {3} {4}";

            var distMins = this.Minutes.Distinct();
            var distHours = this.Hours.Distinct();
            var distDaysOfMonth = this.DaysOfMonth.Distinct();
            var distMonths = this.Months.Distinct();
            var distDOW = this.DaysOfWeek.Distinct();

            var minExpr = distMins.Count() == CronParser.MINUTE_RANGE.Length ?
                            CronParser.ANY_TOKEN.ToString() :  //YES
                            String.Join(CronParser.LIST_TOKEN.ToString(), distMins); //NO

            var hoursExpr = distHours.Count() == 0 ||
                          distHours.Count() == CronParser.HOUR_RANGE.Length ?
                            CronParser.ANY_TOKEN.ToString() : //YES
                            String.Join(CronParser.LIST_TOKEN.ToString(), distHours); //NO

            var domExpr = distDaysOfMonth.Count() == 0 ||
                          distDaysOfMonth.Count() == CronParser.DAY_RANGE.Length ?
                            CronParser.ANY_TOKEN.ToString() : //YES
                            String.Join(CronParser.LIST_TOKEN.ToString(), distDaysOfMonth); //NO

            var monthExpr = distMonths.Count() == 0 ||
                          distMonths.Count() == CronParser.MONTH_RANGE.Length ? 
                            CronParser.ANY_TOKEN.ToString() : //YES
                            String.Join(CronParser.LIST_TOKEN.ToString(), distMonths); //NO

            var dowExpr = distDOW.Count() == 0 ||
                          distDOW.Count() == CronParser.DOW_RANGE.Length ? 
                            CronParser.ANY_TOKEN.ToString() : //YES
                            String.Join(CronParser.LIST_TOKEN.ToString(), distDOW); //NO

            return String.Format(fmt, minExpr, hoursExpr, domExpr, monthExpr, dowExpr);
        }
    }
}
