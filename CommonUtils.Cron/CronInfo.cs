﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonUtils.Cron
{
    public class CronInfo
    {
        public List<DayOfWeek> DaysOfWeekT { get; set; }
        public List<ushort> DaysOfWeek { get; set; }
        public List<ushort> Months { get; set; }
        public List<ushort> DaysOfMonth { get; set; }
        public List<ushort> Hours { get; set; }
        public List<ushort> Minutes { get; set; }

        public string Expr { get; set; }
        public string ExprDaysOfWeek { get; set; }
        public string ExprMonths { get; set; }
        public string ExprHours { get; set; }
        public string ExprMinutes { get; set; }
        public string ExprDaysOfMonth { get; set; }

        public CronInfo()
        {

        }

        public CronInfo(string expr)
        {
            var p = CronParser.Instance.Parse(expr);

            this.DaysOfMonth = p.DaysOfMonth;
            this.DaysOfWeek = p.DaysOfWeek;
            this.Expr = p.Expr;
            this.ExprDaysOfMonth = p.ExprDaysOfMonth;
            this.ExprDaysOfWeek = p.ExprDaysOfWeek;
            this.ExprHours = p.ExprHours;
            this.ExprMinutes = p.ExprMinutes;
            this.ExprMonths = p.ExprMonths;
            this.Hours = p.Hours;
            this.Minutes = p.Minutes;
            this.Months = p.Months;
        }

        public DateTime? NextRun()
        {
            DateTime? dt = null;

            var now = DateTime.Now;

            var monthPassed = this.Months.Count == 0 || this.Months.Contains(Convert.ToUInt16(now.Month));
            var domPassed = this.DaysOfMonth.Count == 0 || this.DaysOfMonth.Contains(Convert.ToUInt16(now.Day));
            var dowPassed = this.DaysOfWeekT.Count == 0 || this.DaysOfWeekT.Contains(now.DayOfWeek);
            var hoursPassed = this.Hours.Count == 0 || this.Hours.Contains(Convert.ToUInt16(now.TimeOfDay.Hours));
            var minsPassed = this.Minutes.Count == 0 || this.Minutes.Contains(Convert.ToUInt16(now.TimeOfDay.Minutes));


            var min = now.TimeOfDay.Minutes;
            var hour = now.TimeOfDay.Hours;
            var day = now.Day;
            var month = now.Month;

            dt = new DateTime(now.Year, month, day, hour, min, 0).AddMinutes(1);

            dt = IncrementDateRecursive(dt);

            return dt;
        }

        private DateTime? IncrementDateRecursive(DateTime? dt)
        {
            var inDate = dt.Value.Date;

            // Month
            while (!this.Months.Contains(Convert.ToUInt16(dt.Value.Month)))
            {
                var rDate = dt.Value.AddMonths(1).Date;
                dt = new DateTime(rDate.Year, rDate.Month, 1);
            }
            if (dt.Value.Date != inDate) return IncrementDateRecursive(dt);

            // Day
            while (!this.DaysOfMonth.Contains(Convert.ToUInt16(dt.Value.Day)))
            { dt = dt.Value.AddDays(1).Date; }
            if (dt.Value.Date != inDate) return IncrementDateRecursive(dt);

            // Week day
            while (!this.DaysOfWeekT.Contains(dt.Value.DayOfWeek))
            { dt = dt.Value.AddDays(1).Date; }
            if (dt.Value.Date != inDate) return IncrementDateRecursive(dt);

            // Hours
            while (!this.Hours.Contains(Convert.ToUInt16(dt.Value.TimeOfDay.Hours)))
            {
                dt = dt.Value.AddHours(1);
                dt = dt.Value.AddMinutes(-dt.Value.TimeOfDay.TotalMinutes % 60);
            }
            if (dt.Value.Date != inDate) return IncrementDateRecursive(dt);

            //Minute
            while (!this.Minutes.Contains(Convert.ToUInt16(dt.Value.TimeOfDay.Minutes)))
            { dt = dt.Value.AddMinutes(1); }
            if (dt.Value.Date != inDate) return IncrementDateRecursive(dt);

            return dt;
        }
    }
}
