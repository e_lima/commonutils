﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtils.Cron.Exceptions;

namespace CommonUtils.Cron
{
    public class CronParser
    {
        internal const char SEGMENTS_TOKEN = '/';
        internal const char RANGE_TOKEN = '-';
        internal const char LIST_TOKEN = ',';
        internal const char ANY_TOKEN = '*';

        static object _sync = new object();
        static CronParser _instance = new CronParser();

        internal static readonly ushort[] MINUTE_RANGE = null;
        internal static readonly ushort[] SECOND_RANGE = null;
        internal static readonly ushort[] DAY_RANGE = null;
        internal static readonly ushort[] MONTH_RANGE = null;
        internal static readonly ushort[] HOUR_RANGE = null;
        internal static readonly ushort[] DOW_RANGE = null;

        public static CronParser Instance
        {
            get
            {
                lock (_sync)
                {
                    return _instance;
                }
            }
        }

        static CronParser()
        {
            MINUTE_RANGE = new ushort[] { 
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 
                40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
                50, 51, 52, 53, 54, 55, 56, 57, 58, 59 
            };

            SECOND_RANGE = new ushort[] { 
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 
                40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
                50, 51, 52, 53, 54, 55, 56, 57, 58, 59 
            };

            DAY_RANGE = new ushort[] { 
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31
            };

            MONTH_RANGE = new ushort[] { 
                1, 2, 3, 4, 5, 6, 7, 8, 9, 
                10, 11, 12
            };

            HOUR_RANGE = new ushort[] { 
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
                20, 21, 22, 23
            };

            DOW_RANGE = new ushort[] { 
                0, 1, 2, 3, 4, 5, 6
            };
        }

        public static CronInfo ParseExpr(string expr)
        {
            return Instance.Parse(expr ?? String.Format("{0} {0} {0} {0} {0}", ANY_TOKEN));
        }

        public CronInfo Parse(string expr)
        {
            try
            {
                var ci = new CronInfo();

                var parts = expr.Split(' ');

                var minutesExpr = parts[0];
                var hoursExpr = parts[1];
                var daysOfMonthExpr = parts[2];
                var monthsExpr = parts[3];
                var daysOfWeekExpr = parts[4];

                ci.ExprMinutes = minutesExpr;
                ci.ExprHours = hoursExpr;
                ci.ExprDaysOfMonth = daysOfMonthExpr;
                ci.ExprMonths = monthsExpr;
                ci.ExprDaysOfWeek = daysOfWeekExpr;

                ci.Minutes = ParseRangeExpr(MINUTE_RANGE, ci.ExprMinutes, ushort.TryParse);
                ci.Hours = ParseRangeExpr(HOUR_RANGE, ci.ExprHours, ushort.TryParse);
                ci.Months = ParseRangeExpr(MONTH_RANGE, ci.ExprMonths, ushort.TryParse);
                ci.DaysOfMonth = ParseRangeExpr(DAY_RANGE, ci.ExprDaysOfMonth, ushort.TryParse);
                ci.DaysOfWeek = ParseRangeExpr(DOW_RANGE, ci.ExprDaysOfWeek, ushort.TryParse);

                //Translate DayyOfWeek
                this.TranslateDaysOfWeek(ci);
                return ci;
            }
            catch (IndexOutOfRangeException ioorEx)
            {
                throw new CronParserException(String.Format("Invalid number of arguments in expression [{0}]", expr), ioorEx);
            }
            catch (Exception ex)
            {
                throw new CronParserException(String.Format("Error parsing: {0}", expr), ex);
            }
        }

        private void TranslateDaysOfWeek(CronInfo ci)
        {
            ci.DaysOfWeekT = new List<DayOfWeek>(ci.DaysOfWeek.Count);
            foreach (var item in ci.DaysOfWeek)
            {
                switch (item % 7)
                {
                    case 0:
                    case 7:
                        ci.DaysOfWeekT.Add(DayOfWeek.Sunday);
                        break;
                    case 1:
                        ci.DaysOfWeekT.Add(DayOfWeek.Monday);
                        break;
                    case 2:
                        ci.DaysOfWeekT.Add(DayOfWeek.Tuesday);
                        break;
                    case 3:
                        ci.DaysOfWeekT.Add(DayOfWeek.Wednesday);
                        break;
                    case 4:
                        ci.DaysOfWeekT.Add(DayOfWeek.Thursday);
                        break;
                    case 5:
                        ci.DaysOfWeekT.Add(DayOfWeek.Friday);
                        break;
                    case 6:
                        ci.DaysOfWeekT.Add(DayOfWeek.Saturday);
                        break;
                    default:
                        break;
                }
            }
            ci.DaysOfWeekT = ci.DaysOfWeekT.Distinct().ToList();
        }

        //private List<ushort> ParseHours(string p)
        //{
        //    var list = new List<ushort>();
        //    var parts = p.Split(LIST_TOKEN);

        //    foreach (var part in parts)
        //    {
        //        ushort v = 0;

        //        if (part == ANY_TOKEN.ToString()) // all values
        //        {
        //            list.AddRange(HOUR_RANGE);
        //            break;
        //        }
        //        else if (ushort.TryParse(part, out v)) //single value
        //        {
        //            list.Add(v);
        //        }
        //        else if (part.Contains(SEGMENTS_TOKEN)) //mixed-range value 
        //        {
        //            var segs = part.Split(SEGMENTS_TOKEN);

        //            var part2 = segs[0];
        //            var incr = ushort.Parse(segs[1]);

        //            var definedRange = ParseRange(HOUR_RANGE, part2);
        //            list.AddRange(ApplyIncr(definedRange, incr));
        //        }
        //        else if (part.Contains(RANGE_TOKEN)) //range value 
        //        {
        //            list.AddRange(ParseRange(HOUR_RANGE, part));
        //        }

        //    }

        //    return list;
        //}

        internal List<T> ParseRangeExpr<T>(IEnumerable<T> range, string p, ParseFunc<T> func) where T : IComparable
        {
            var list = new List<T>();
            var parts = p.Split(LIST_TOKEN);

            foreach (var part in parts)
            {
                T v;

                if (part == ANY_TOKEN.ToString()) // all values
                {
                    list.AddRange(range);
                    break;
                }
                else if (func(part, out v)) //single value
                {
                    list.Add(v);
                }
                else if (part.Contains(SEGMENTS_TOKEN)) //mixed-range value 
                {
                    var segs = part.Split(SEGMENTS_TOKEN);

                    var part2 = segs[0];
                    var incr = ushort.Parse(segs[1]);

                    var definedRange = ParseRange(range, part2, func);
                    list.AddRange(ApplyIncr(definedRange, incr));
                }
                else if (part.Contains(RANGE_TOKEN)) //range value 
                {
                    list.AddRange(ParseRange(range, part, func));
                }
            }

            return list.Distinct().ToList();
        }

        private IEnumerable<T> ApplyIncr<T>(IEnumerable<T> definedRange, ushort incr) where T : IComparable
        {
            var list = new List<T>();
            var curr = 0;
            var max = definedRange.Count();

            do
            {
                list.Add(definedRange.ElementAt(curr));
                curr += incr;
            } while (curr < max);

            return list;
        }

        private static IEnumerable<T> ParseRange<T>(IEnumerable<T> range, string expr, ParseFunc<T> func) where T : IComparable
        {
            var rangeParts = expr.Split(RANGE_TOKEN);

            if (rangeParts.Length > 1)
            {
                var definedRange = range.Where(ri =>
                {

                    T min;
                    T max;

                    func(rangeParts[0], out min);
                    func(rangeParts[1], out max);

                    return ri.CompareTo(min) >= 0 && ri.CompareTo(max) <= 0;
                });
                return definedRange;
            }
            else
            {
                return range;
            }
        }
    }

    public delegate bool ParseFunc<T>(string s, out T parsed);

}
