﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommonUtils.Cron
{
    public class CronScheduler : IDisposable
    {
        List<CronTask> Schedules { get; set; }
        Timer _pollTimer;

        public long UpdatedAt { get; private set; }

        bool _enabled;
        public bool Enabled {
            get
            {
                return _enabled;
            }
            set
            {
                if (!_enabled && value)
                    _pollTimer = new Timer(new TimerCallback(pollTimer_Callback), Schedules, 50, 200);

                if (_enabled && !value) {
                    if (_pollTimer != null) {
                        _pollTimer.Dispose();
                        this.IsProcessing = false;
                    }
                }

                _enabled = value;
            }
        }

        public CronScheduler()
        {
            Schedules = new List<CronTask>();
        }

        public void AddTask(string expression, Action task)
        {
            var ci = CronParser.ParseExpr(expression);
            this.Schedules.Add(new CronTask { CronInfo = ci, TaskAction = task, NextRun = ci.NextRun().Value });
        }

        public void AddTask(CronInfo ci, Action task)
        {
            this.Schedules.Add(new CronTask { CronInfo = ci, TaskAction = task, NextRun = ci.NextRun().Value });
        }

        public void AddTask(CronInfo ci, ITask task)
        {
            Action a = () => task.Execute();
            AddTask(ci, a);
        }

        private void pollTimer_Callback(object obj) 
        {
            this.UpdatedAt = DateTime.Now.Ticks;

            if (this.Enabled && !this.IsProcessing)
            {
                this.IsProcessing = true;
                var now = DateTime.Now;

                foreach (var item in Schedules)
                {
                    if ((item.CurrentTask == null || item.CurrentTask.Status != TaskStatus.Running) && this.Enabled) //double check
                    {
                        if ((item.NextRun - now).Milliseconds < 0)
                        {
                            item.CurrentTask = new Task(item.TaskAction);
                            item.CurrentTask.Start();
                            item.NextRun = item.CronInfo.NextRun().Value;
                        }
                    }
                }
                this.IsProcessing = false;
            }
        }

        public void Dispose()
        {
            if (_pollTimer != null)
                _pollTimer.Dispose();

            this.Schedules
                .Where(s => s.CurrentTask != null)
                .ToList()
                .ForEach(s => s.CurrentTask.Dispose());
            this.Schedules.Clear();
        }

        public Task[] GetPendingTasks()
        {
            return this
                .Schedules
                .Where(s => s.CurrentTask != null && s.CurrentTask.Status == TaskStatus.Running)
                .Select(s => s.CurrentTask)
                .ToArray();
        }

        public bool IsProcessing { get; set; }

        public void Enable()
        {
            this.Enabled = true;
        }

        public void Disable()
        {
            this.Enabled = false;
        }
    }
}
