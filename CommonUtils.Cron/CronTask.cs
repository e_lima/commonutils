﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommonUtils.Cron
{
    public class CronTask
    {
        public Action TaskAction { get; set; }
        public CronInfo CronInfo { get; set; }
        public DateTime NextRun { get; set; }
        public Task CurrentTask { get; set; }

        public CronTask()
        {
        }
    }
}
