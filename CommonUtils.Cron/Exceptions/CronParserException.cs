﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CommonUtils.Cron.Exceptions
{
    public class CronParserException : Exception
    {
        public CronParserException() : base()
        {
        }

        public CronParserException(String message) : base(message)
        {
        }

        public CronParserException(String message, Exception exception) : base(message, exception)
        {
        }
    }
}
