﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonUtils.Cron
{
    public interface ITask
    {
        void Execute();
    }
}
