﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonUtils.Cron;

namespace CommonUtils.CronTests
{
    /// <summary>
    /// Summary description for BuilderTests
    /// </summary>
    [TestClass]
    public class BuilderTests
    {
        [TestMethod]
        public void Builder_Tests()
        {
            var expr = String.Empty;

            Assert.AreEqual(CronFluentBuilder.Build().AddMinutes(1).GetCronInfo().Minutes.Count, 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddMinutes(1,2).GetCronInfo().Minutes.Count, 2);
            Assert.AreEqual(CronFluentBuilder.Build().AddHours(1).GetCronInfo().Hours.Count, 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddHours(1,2).GetCronInfo().Hours.Count, 2);
            Assert.AreEqual(CronFluentBuilder.Build().AddDaysOfMonth(1).GetCronInfo().DaysOfMonth.Count, 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddDaysOfMonth(1,2).GetCronInfo().DaysOfMonth.Count, 2);
            Assert.AreEqual(CronFluentBuilder.Build().AddDaysOfWeek(0).GetCronInfo().DaysOfWeek.Count, 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddDaysOfWeek(0, 1).GetCronInfo().DaysOfWeek.Count, 2);
            Assert.IsTrue(CronFluentBuilder.Build().AddDaysOfWeek(0).GetCronInfo().DaysOfWeekT.Contains(DayOfWeek.Sunday));
            Assert.IsTrue(CronFluentBuilder.Build().AddDaysOfWeek(0,7).GetCronInfo().DaysOfWeekT.Count == 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddMonths(1).GetCronInfo().Months.Count, 1);
            Assert.AreEqual(CronFluentBuilder.Build().AddMonths(1, 2).GetCronInfo().Months.Count, 2);

            //expr = new CronFluentBuilder().SetMinutes("0", "10", "20,30", "30-40", "40-59/2").GetExpr();
            expr = new CronFluentBuilder().GetExpr();
            Assert.AreEqual("* * * * *", expr);

            expr = new CronFluentBuilder().SetMinutes("*").GetExpr();
            Assert.AreEqual("* * * * *", expr);

            expr = new CronFluentBuilder().SetHours("*").GetExpr();
            Assert.AreEqual("* * * * *", expr);

            expr = new CronFluentBuilder().SetMinutes("20,30").GetExpr();
            Assert.AreEqual("20,30 * * * *", expr);

            expr = new CronFluentBuilder().SetMinutes("20-25").GetExpr();
            Assert.AreEqual("20,21,22,23,24,25 * * * *", expr);

            expr = new CronFluentBuilder().SetMinutes("20,30").GetExpr();
            Assert.AreEqual("20,30 * * * *", expr);

            expr = new CronFluentBuilder().SetMinutes("20-25").GetExpr();
            Assert.AreEqual("20,21,22,23,24,25 * * * *", expr);

            expr = new CronFluentBuilder().SetHours("14").GetExpr();
            Assert.AreEqual("* 14 * * *", expr);

            expr = new CronFluentBuilder().SetHours("14-16").GetExpr();
            Assert.AreEqual("* 14,15,16 * * *", expr);

            expr = new CronFluentBuilder().SetHours("14,15,16").GetExpr();
            Assert.AreEqual("* 14,15,16 * * *", expr);

            expr = new CronFluentBuilder().SetHours("14-16/1").GetExpr();
            Assert.AreEqual("* 14,15,16 * * *", expr);

            expr = new CronFluentBuilder()
                .SetDaysOfWeek(5)
                .SetHours(2)
                .SetMinutes(1)
                .SetMonths(4)
                .SetDaysOfMonth(3).GetExpr();

            Assert.AreEqual("1 2 3 4 5", expr);

            var ci = CronParser.ParseExpr(expr);
            Assert.AreEqual(1, ci.Hours.Count);
            Assert.AreEqual(2, ci.Hours.Max());
            Assert.AreEqual(2, ci.Hours.Min());

            expr = CronFluentBuilder.Build().SetMinutes(0).GetExpr();
            Assert.AreEqual("0 * * * *", expr);

            expr = CronFluentBuilder.Build()
                .SetMinutes(0)
                .SetDaysOfWeek(DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday)
                .GetExpr();
            Assert.AreEqual("0 * * * 1,2,3,4,5", expr);

            var now = DateTime.Now;
            var dt = CronFluentBuilder.Build().GetCronInfo().NextRun();
            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            var diff = (dt - now);
            Assert.IsTrue(diff.GetValueOrDefault().TotalMinutes <= 1);

            now = DateTime.Now;
            dt = CronFluentBuilder
                .Build()
                .SetMinutes(0)
                .GetCronInfo()
                .NextRun();

            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            diff = dt - now;
            Assert.IsTrue(diff.GetValueOrDefault().TotalHours <= 1);
            Assert.AreEqual(0, dt.Value.TimeOfDay.Minutes);

            now = DateTime.Now;
            dt = CronFluentBuilder
                .Build()
                .SetHours(1)
                .GetCronInfo()
                .NextRun();

            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            diff = dt - now;
            Assert.IsTrue(diff.GetValueOrDefault().TotalDays <= 1);
            Assert.AreEqual(1, dt.Value.TimeOfDay.Hours);

            now = DateTime.Now;
            dt = CronFluentBuilder
                .Build()
                .SetDaysOfMonth(15)
                .GetCronInfo()
                .NextRun();

            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            diff = dt - now;
            Assert.IsTrue(diff.GetValueOrDefault().TotalDays <= 31);
            Assert.AreEqual(15, dt.Value.Day);

            now = DateTime.Now;
            dt = CronFluentBuilder
                .Build()
                .SetDaysOfWeek(DayOfWeek.Sunday)
                .GetCronInfo()
                .NextRun();

            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            diff = dt - now;
            Assert.IsTrue(diff.GetValueOrDefault().TotalDays <= 7);
            Assert.AreEqual(DayOfWeek.Sunday, dt.Value.DayOfWeek);

            now = DateTime.Now;
            dt = CronFluentBuilder
                .Build()
                .SetMinutes(30)
                .SetHours(1)
                .SetDaysOfWeek(DayOfWeek.Tuesday)
                .GetCronInfo()
                .NextRun();

            Assert.IsNotNull(dt);
            Assert.AreNotEqual(now, dt);
            Assert.IsTrue(dt > now);
            diff = dt - now;
            Assert.IsTrue(diff.GetValueOrDefault().TotalDays <= 7);
            Assert.AreEqual(DayOfWeek.Tuesday, dt.Value.DayOfWeek);
            Assert.AreEqual(1, dt.Value.TimeOfDay.Hours);
            Assert.AreEqual(30, dt.Value.TimeOfDay.Minutes);
        }
    }
}