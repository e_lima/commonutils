﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonUtils.CronTests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void Test()
        {
            var ci1 = Cron.CronParser.ParseExpr(null);
            Assert.IsTrue(ci1.Minutes.Count == 60);
            Assert.IsTrue(ci1.Minutes.First() == 0);
            Assert.IsTrue(ci1.Minutes.Last() == 59);
            Assert.IsTrue(ci1.Hours.Count == 24);
            Assert.IsTrue(ci1.Hours.First() == 0);
            Assert.IsTrue(ci1.Hours.Last() == 23);
            Assert.IsTrue(ci1.Months.Count == 12);
            Assert.IsTrue(ci1.Months.First() == 1);
            Assert.IsTrue(ci1.Months.Last() == 12);
            Assert.IsTrue(ci1.DaysOfMonth.Count == 31);
            Assert.IsTrue(ci1.DaysOfMonth.First() == 1);
            Assert.IsTrue(ci1.DaysOfMonth.Last() == 31);
            Assert.IsTrue(ci1.DaysOfWeekT.Count == 7);
            Assert.IsTrue(ci1.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(ci1.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            ci1 = Cron.CronParser.ParseExpr("* 6,7 * * *");
            Assert.IsTrue(ci1.Hours.Count == 2);

            try
            {
                Cron.CronParser.ParseExpr("xpto");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(Cron.Exceptions.CronParserException));
            }

            var c = Cron.CronParser.ParseExpr("* * * * *");
            Assert.IsTrue(c.Minutes.Count == 60);
            Assert.IsTrue(c.Minutes.First() == 0);
            Assert.IsTrue(c.Minutes.Last() == 59);
            Assert.IsTrue(c.Hours.Count == 24);
            Assert.IsTrue(c.Hours.First() == 0);
            Assert.IsTrue(c.Hours.Last() == 23);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);
            
            c = Cron.CronParser.ParseExpr("10 * * * *");
            Assert.IsTrue(c.Minutes.Count == 1);
            Assert.IsTrue(c.Minutes[0] == 10);
            Assert.IsTrue(c.Hours.Count == 24);
            Assert.IsTrue(c.Hours.First() == 0);
            Assert.IsTrue(c.Hours.Last() == 23);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10 * * * *");
            Assert.IsTrue(c.Minutes.Count == 6);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 10);
            Assert.IsTrue(c.Hours.Count == 24);
            Assert.IsTrue(c.Hours.First() == 0);
            Assert.IsTrue(c.Hours.Last() == 23);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 * * * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 24);
            Assert.IsTrue(c.Hours.First() == 0);
            Assert.IsTrue(c.Hours.Last() == 23);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 20 * * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 1);
            Assert.IsTrue(c.Hours.First() == 20);
            Assert.IsTrue(c.Hours.Last() == 20);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20 * * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 5);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 20);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/2 * * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 3);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 20);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 * * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 31);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 1 * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 1);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 1);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 1-3 * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 3);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 3);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 * *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 12);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 12);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 3 *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 1);
            Assert.IsTrue(c.Months.First() == 3);
            Assert.IsTrue(c.Months.Last() == 3);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 3,9 *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 2);
            Assert.IsTrue(c.Months.First() == 3);
            Assert.IsTrue(c.Months.Last() == 9);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 */3,*/5 *");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 6);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 11);
            Assert.IsTrue(c.DaysOfWeek.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.Count == 7);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 */3,*/5 0");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 6);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 11);
            Assert.IsTrue(c.DaysOfWeek.Count == 1);
            Assert.IsTrue(c.DaysOfWeekT.Count == 1);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Sunday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 */3,*/5 0,3");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 6);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 11);
            Assert.IsTrue(c.DaysOfWeek.Count == 2);
            Assert.IsTrue(c.DaysOfWeekT.Count == 2);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Wednesday);

            c = Cron.CronParser.ParseExpr("5-10/3 16-20/3 */3 */3,*/5 */3");
            Assert.IsTrue(c.Minutes.Count == 2);
            Assert.IsTrue(c.Minutes.Min() == 5);
            Assert.IsTrue(c.Minutes.Max() == 8);
            Assert.IsTrue(c.Hours.Count == 2);
            Assert.IsTrue(c.Hours.First() == 16);
            Assert.IsTrue(c.Hours.Last() == 19);
            Assert.IsTrue(c.DaysOfMonth.Count == 11);
            Assert.IsTrue(c.DaysOfMonth.First() == 1);
            Assert.IsTrue(c.DaysOfMonth.Last() == 31);
            Assert.IsTrue(c.Months.Count == 6);
            Assert.IsTrue(c.Months.First() == 1);
            Assert.IsTrue(c.Months.Last() == 11);
            Assert.IsTrue(c.DaysOfWeek.Count == 3);
            Assert.IsTrue(c.DaysOfWeekT.Count == 3);
            Assert.IsTrue(c.DaysOfWeekT.First() == DayOfWeek.Sunday);
            Assert.IsTrue(c.DaysOfWeekT.Last() == DayOfWeek.Saturday);
        } 
    }
}
