﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonUtils.Cron;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace CommonUtils.CronTests
{
    /// <summary>
    /// Summary description for SchedulerTests
    /// </summary>
    [TestClass]
    public class SchedulerTests
    {
        public SchedulerTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [MTAThread]
        public void Scheduler_Tests()
        {
            using (var scheduler = new CronScheduler())
            {
                Assert.AreEqual(0, scheduler.UpdatedAt);
                Assert.AreEqual(false, scheduler.Enabled);
                Assert.AreNotEqual(true, scheduler.IsProcessing);

                var now1 = DateTime.Now;
                //gets the 2 next minute

                //execute 2 times in 20 secs
                var runCount = 0;
                scheduler.AddTask(String.Format("{0},{1} * * * *", now1.Minute+1, now1.Minute+2), () => runCount++);
                scheduler.Enable();
                Thread.Sleep(60 * 5 * 1000);
                Assert.AreEqual(2, runCount);
                scheduler.Disable();

                scheduler.AddTask(CronParser.ParseExpr("* * * * *"), () =>
                {
                    Assert.AreEqual(true, scheduler.Enabled);
                    Debug.WriteLine("Task 1: Begin");
                    Thread.Sleep(10 * 1000);
                    Debug.WriteLine("Task 1: End");
                });

                Assert.AreEqual(false, scheduler.Enabled);
                scheduler.AddTask(CronParser.ParseExpr("* * * * *"), () =>
                {
                    Assert.AreEqual(true, scheduler.Enabled);
                    Debug.WriteLine("Task 2: Begin");
                    Thread.Sleep(20 * 1000);
                    Debug.WriteLine("Task 2: End");
                });

                var beforeWait = DateTime.Now;
                var tasks = scheduler.GetPendingTasks();
                Assert.AreEqual(0, tasks.Count());
                Task.WaitAll(tasks);
                var afterWait = DateTime.Now;

                Assert.IsTrue((afterWait - beforeWait).Seconds < 10);
                Assert.AreNotEqual(true, scheduler.IsProcessing);

                scheduler.Enabled = true;
                Thread.Sleep(300);
                Assert.AreNotEqual(0, scheduler.UpdatedAt);

                var oldUpdatedAt = scheduler.UpdatedAt;

                //First execution
                Debug.WriteLine("-------------------------------");
                Debug.WriteLine("First run:");
                Debug.WriteLine("-------------------------------");
                var now = DateTime.Now;
                Debug.WriteLine("Current Time: {0}", now.TimeOfDay);
                var waitFor = 61000 - (int)((DateTime.Now.TimeOfDay.TotalSeconds % 60) * 1000);
                Debug.WriteLine("Wait for: {0}", waitFor);
                Assert.AreNotEqual(0, waitFor);
                Thread.Sleep(waitFor);

                beforeWait = DateTime.Now;
                tasks = scheduler.GetPendingTasks();
                Assert.AreEqual(2, tasks.Count());
                Debug.WriteLine("Waiting for tasks completion ...");
                Task.WaitAll(tasks);
                afterWait = DateTime.Now;
                Assert.IsTrue((afterWait - beforeWait).Seconds >= 18);

                //Check UpdatedAt change
                Assert.AreNotEqual(oldUpdatedAt, scheduler.UpdatedAt);

                //No execution (wait for a minute)
                Debug.WriteLine("-------------------------------");
                Debug.WriteLine("No run (wait for the next minute): ");
                Debug.WriteLine("-------------------------------");
                tasks = scheduler.GetPendingTasks();
                Debug.WriteLine("Waiting for tasks completion ...");
                Task.WaitAll(tasks);
                Assert.AreEqual(0, tasks.Length);

                //Second execution
                Debug.WriteLine("-------------------------------");
                Debug.WriteLine("Second run: ");
                Debug.WriteLine("-------------------------------");
                now = DateTime.Now;
                Debug.WriteLine("Current Time: {0}", now.TimeOfDay);
                waitFor = 61000 - (int)((DateTime.Now.TimeOfDay.TotalSeconds % 60) * 1000);
                Debug.WriteLine("Wait for: {0}", waitFor);
                
                Assert.AreNotEqual(0, waitFor);
                Thread.Sleep(waitFor);

                beforeWait = DateTime.Now;
                tasks = scheduler.GetPendingTasks();
                Assert.AreEqual(2, tasks.Count());
                Debug.WriteLine("Waiting for tasks completion ...");
                Task.WaitAll(tasks);
                afterWait = DateTime.Now;
                Assert.IsTrue((afterWait - beforeWait).Seconds >= 18);

                Debug.WriteLine("-------------------------------");
                Debug.WriteLine("Finish");
                Debug.WriteLine("-------------------------------");
            }
        }
    }
}
