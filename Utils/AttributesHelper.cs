﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace CommonUtils.Utils
{
    public static class AttributesHelper
    {
        public static string GetDisplayName(Type t)
        {
            var displayName = t.Name;
            
            var att = t.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault();
            if (att is DisplayNameAttribute) displayName = (att as DisplayNameAttribute).DisplayName;

            return displayName;
        }

        public static string GetDisplayName(PropertyInfo pi)
        {
            var displayName = pi.Name;

            var att = pi.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault();
            if (att is DisplayNameAttribute) displayName = (att as DisplayNameAttribute).DisplayName;

            return displayName;

        }

        public static string GetDisplayName(MethodInfo mi)
        {
            var displayName = mi.Name;
            
            var att = mi.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault();
            if (att is DisplayNameAttribute) displayName = (att as DisplayNameAttribute).DisplayName;

            return displayName;

        }
    }
}
