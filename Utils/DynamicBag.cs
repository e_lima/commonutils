﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.Globalization;
using System.Threading;
using System.IO;

namespace ResourceBag.ResourceBag
{
    public class DynamicBag : DynamicObject
    {
        Dictionary<CultureInfo, Dictionary<String, String>> _CultureIndex = new Dictionary<CultureInfo, Dictionary<string, string>>();
        List<String> _AllKeys = new List<string>();

        public DynamicBag()
        {
            foreach (var file in Directory.EnumerateFiles("Resources\\", "*.rbag.*"))
            {
                var ext = Path.GetExtension(file);
                var filename = Path.GetFileNameWithoutExtension(file);

                var contents = File.ReadAllLines(file);

                var dictionary = new Dictionary<string, string>();
                var ci = CultureInfo.GetCultureInfo(ext.Trim('.'));
                foreach (var item in contents)
                {
                    var pair = item.Split(':');
                    dictionary.Add(pair[0], pair[1].Trim());

                    if (!_AllKeys.Contains(pair[0]))
                        _AllKeys.Add(pair[0]);
                }

                _CultureIndex[ci] = dictionary;
            }
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this[binder.Name] = Convert.ToString(value);

            return true;
        }

        private Dictionary<string, string> GetCurrentDictionary()
        {
            var c = Thread.CurrentThread.CurrentUICulture;
            Dictionary<string, string> dict = null;
            _CultureIndex.TryGetValue(c, out dict);

            if (dict == null)
            {
                dict = new Dictionary<string, string>();
                _CultureIndex[c] = dict;
            }

            return dict;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = this[binder.Name];

            return true;
        }

        public IEnumerable<CultureInfo> GetCultures()
        {
            return _CultureIndex.Keys;
        }

        public DynamicBag AddCultureDictionary(CultureInfo ci, Dictionary<string, string> cultureDictionary)
        {
            _CultureIndex[ci] = cultureDictionary ?? new Dictionary<string, string>();

            return this;
        }

        public DynamicBag AddCultureDictionary(string cultureName, Dictionary<string, string> cultureDictionary)
        {
            var ci = CultureInfo.GetCultureInfo(cultureName);
            _CultureIndex[ci] = cultureDictionary ?? new Dictionary<string, string>();

            return this;
        }

        public string[] GetAllKeys()
        {
            return _AllKeys.ToArray();
        }

        public string this[string key]
        {
            get
            {
                var dict = GetCurrentDictionary();
                var value = String.Empty;

                if (!dict.TryGetValue(key, out value))
                    value =  "::" + key + "::";

                return value;
            }
            set
            {
                GetCurrentDictionary()[key] = value;
            }
        }
    }
}
