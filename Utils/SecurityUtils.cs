﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;

namespace CommonUtils.Utils
{
    public static class SecurityUtils
    {
        public static string SaltedHash(string text)
        {
            var salt = ConfigurationManager.AppSettings["security.salt"] ?? "some.salt.123";

            var passBytes = Encoding.Default.GetBytes(text);
            var saltBytes = Encoding.Default.GetBytes(salt);
            var fullBytes = new byte[passBytes.Length + saltBytes.Length];
            Buffer.BlockCopy(passBytes, 0, fullBytes, 0, passBytes.Length);
            Buffer.BlockCopy(saltBytes, 0, fullBytes, passBytes.Length, saltBytes.Length);

            var alg = HashAlgorithm.Create("SHA1");
            var encBytes = alg.ComputeHash(fullBytes.ToArray());

            return Convert.ToBase64String(encBytes);
        }
    }
}
